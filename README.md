# Angular course code examples repo

## Installing

Node.js ^8.10.0 version is recommended. Use [nvm](https://github.com/creationix/nvm) for local node versioning

### Local env

#### With Webpack dev server
```
npm install

npm run start

```

App will be available at [http://localhost:8080/](http://localhost:8080/)
