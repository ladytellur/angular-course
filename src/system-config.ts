declare const System: any;

System.config({
  paths: {
    'node:*': 'node_modules/*'
  },
  map: {
    'rxjs': 'node:rxjs',
    'main': 'main.ts',
  },
  packages: {
    'rxjs': {main: 'index'},

    '.': {
      defaultExtension: 'js'
    }
  }
});
