import { Component } from '@angular/core';

@Component({
  selector: 'demo-card-region',
  templateUrl: './card-region.component.html'
})

export class CardRegionComponent {
  public cardData = [
    {
      title: 'Card 1 title',
      text: 'Card 1 text'
    },
    {
      title: 'Card 2 title',
      text: 'Card 2 text'
    }
  ];

  constructor() {}

  updateText() {
    this.cardData[1].text = 'New amazing updated text ' + Math.random();
  }

  updateTitle() {
    this.cardData[1].title = 'New amazing updated title ' + Math.random();
  }
}
