import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { Routing } from './app.routing';

import { CardComponent } from './card/card.component';
import { CardRegionComponent } from './card-region/card-region.component';

@NgModule({
  imports: [
    Routing,
    CommonModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    CardComponent,
    CardRegionComponent
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})

export class AppModule {}
