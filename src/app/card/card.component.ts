import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'demo-card',
  templateUrl: './card.component.html'
})

export class CardComponent implements OnInit {

  @Input('title') title: string;
  @Input('text') text: string;

  constructor() {
    console.log('text will be empty string here', this.text);
  }

  ngOnInit() {
    console.log('OnInit this._text', this.text);
  }
}
