import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = <any>[
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);
