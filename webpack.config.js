const path = require('path');
const rxPaths = require('rxjs/_esm5/path-mapping');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const ROOT = path.resolve(__dirname);

console.log('DEVSERVER');

module.exports = {
  devtool: 'source-map',

  performance: {
    hints: false
  },

  entry: {
    'polyfills': ROOT + '/src/polyfills.ts',
    'vendor': ROOT + '/src/vendor.ts',
    'app': ROOT + '/src/main.ts'
  },

  output: {
    path: ROOT + '/public/',
    filename: 'dist/[name].bundle.js',
    chunkFilename: 'dist/[id].chunk.js',
    publicPath: '/'
  },

  resolve: {
    extensions: ['.ts', '.js', '.json'],
    alias: rxPaths()
  },

  devServer: {
    historyApiFallback: true,
    contentBase: ROOT + '/public/',
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    },
    port: 8080
  },

  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          'awesome-typescript-loader',
          'angular-router-loader?debug:true',
          'angular2-template-loader',
          'source-map-loader',
          'tslint-loader'
        ]
      },
      {
        test: /\.html$/,
        use: 'raw-loader'
      }
    ],
    exprContextCritical: false
  },
  plugins: [
    function() {
      this.plugin('watch-run',
        function(watching, callback) {
          console.log('\x1b[33m%s\x1b[0m', `Begin compile at ${(new Date()).toTimeString()}`);
          callback();
        });
    },

    new CleanWebpackPlugin(
      [
        './public/dist/*.js',
        './public/dist/*.map'
      ],
      { root: ROOT }
    ),

    new HtmlWebpackPlugin({
      filename: 'index.html',
      inject: 'body',
      template: 'src/public/index.html'
    }),

    new CopyWebpackPlugin([
      { from: './src/public/index.html', to: '', flatten: true }
    ])
  ]
};
